python edit_data.py --file /lustre/projects/DTAdb/resources/epigenetics/sqlite_db/test_file_1.txt \
--delimiter '|' --table 'files' --operation 'update_column' --columns disease --types STRING --dict_cols 1 10 --add_col True



python edit_data.py --file /home/rmgpccl/epigenetics_db/snATAC_Cardiac_gtex.txt --delimiter '|' --table 'atac_seq' --operation 'update_column' --based_on cluster --columns gtex_relation --types STRING --dict_cols 0 1 --add_col True




while read f
do
echo $f
python /home/rmgpccl/epigenetics_db/epigenetics_db/enter_data_db.py --file /lustre/projects/DTAdb/resources/epigenetics/diHMM/data/${f}_hg38.bed.gz --tissue ${f} --source "https://github.com/gcyuan/diHMM-cpp" --regulatory_category 'ChromatinState' --gtex_relation diHMM_files_GTEx --compression 'gzip' --state_col 3
# python epigenetics_db/edit_data.py --operation 'delete_file' --based_on file_basename --based_on_val ${f}.bed.gz
done < diHMM_files


python query.py --file ~/sqtls/positive_controls/results_positive_controls_ctd.csv --delimiter ',' --lift_from GRCh37 --lift_to GRCh38 --liftover True --lift_from GRCh38 --lift_to GRCh37 --file_type list_of_uni_ids --variants_col 'Variants' --header True



