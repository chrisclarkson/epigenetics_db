.. <PACKAGE> documentation master file, created by
   sphinx-quickstart on <DAY> <MONTH> <DATE> <TIME> 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to <PACKAGE>
====================

Description...

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Example code

   examples

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   scripts
   api

.. toctree::
   :maxdepth: 2
   :caption: Project admin

   contributing
   package_management

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
