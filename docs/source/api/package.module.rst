<PACKAGE> package
=================

<PACKAGE>.<MODULE> module
-------------------------

.. automodule:: <PACKAGE>.<MODULE>
   :members:
   :undoc-members:
   :show-inheritance:
