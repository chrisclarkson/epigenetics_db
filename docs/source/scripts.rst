======================
Command-line endpoints
======================

Below is a list of all the command line endpoints installed with the <PACKAGE>. Many of these are mentioned in their respective context throughout the documentation but they are listed here all in a single place.

----------------
Python endpoints
----------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   scripts/example_python_script

----------------
Bash endpoints
----------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   scripts/example_bash_script

