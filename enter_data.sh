#!/bin/bash -l
# Batch script to run a serial job under SGE.
#$ -l h_rt=24:00:0
#$ -l mem=10G
#$ -N enter_data
#$ -wd /home/rmgpccl/epigenetics_db/

source ~/.bashrc
conda activate merit
cd /lustre/home/rmgpccl/epigenetics_db

# while read f
# do
# echo $f
# python /home/rmgpccl/epigenetics_db/epigenetics_db/enter_data_db.py --file /lustre/projects/DTAdb/resources/epigenetics/EPIMAP/${f}_18_CALLS_segments.bed.gz --tissue ${f} --source "http://compbio.mit.edu/epimap/#tracks-download-and-visualization" --regulatory_category 'ChromatinState' --state_dict chrhmm_epimap.json --gtex_relation epimap_files_GTEx --compression 'gzip'
# done < epimap_files

while read f
do
echo $f
python /home/rmgpccl/epigenetics_db/epigenetics_db/enter_data_db.py --file /lustre/projects/DTAdb/resources/epigenetics/diHMM/data/${f}_hg38.bed.gz --tissue ${f} --source "https://github.com/gcyuan/diHMM-cpp" --regulatory_category 'ChromatinState' --gtex_relation diHMM_files_GTEx --compression 'gzip' --state_col 3
done < diHMM_files


# python ~/epigenetics_db/epigenetics_db/enter_data_db.py --file /lustre/projects/DTAdb/resources/epigenetics/disease_specific/AD/Marzi_H3K27ac/Marzi_H3K27ac_EntorhinalCortex_38_ctd.bed --tissue "Entorhinal Cortex" --regulatory_category ChIPSeq --source "https://pubmed.ncbi.nlm.nih.gov/30349106/"

# python ~/epigenetics_db/epigenetics_db/enter_data_db.py --file /lustre/projects/DTAdb/resources/epigenetics/disease_specific/Cardiac_disease/Supplemental_Table_5_RAS.tsv --tissue "Cardiac" --regulatory_category ATACSeq --cluster_col 12 --source "http://cepigenomics.org/CARE_portal/#specific-download-links" --state_dict snATAC_Cardiac.json

# python ~/epigenetics_db/epigenetics_db/enter_data_db.py --file /lustre/projects/DTAdb/resources/epigenetics/GVATdb/GVATdb.csv.gz --regulatory_category SNPs_vs_TranscriptionFactor --source "http://renlab.sdsc.edu/GVATdb/search.html" --compression 'gzip' --tf_col 0 --chrpos_spec 2 ":" "-" --pval_col 10 --rsid_col 12 True --sep ',' --header True


# python query.py --file ~/sqtls/positive_controls/results_positive_controls_ctd.csv --delimiter ',' --lift_from GRCh37 --lift_to GRCh38 --liftover True --lift_from GRCh38 --lift_to GRCh37 --file_type list_of_uni_ids --variants_col 'Variants' --header True

# python epigenetics_db/query.py --file test.csv --delimiter ',' --lift_from GRCh37 --lift_to GRCh38 --liftover True --lift_from GRCh38 --lift_to GRCh37 --file_type list_of_uni_ids --variants_col 'Variants' --header True
