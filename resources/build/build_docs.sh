#!/bin/bash
# Run the set of commands to build both HTML and PDF documentation and then
# copy the PDF out of the build directory
cur_dir="$pwd"
prog_name=$(readlink -f "$0")
prog_dir=$(dirname "$prog_name")
cd "$prog_dir"
cp ../../README.md ../../docs/source/getting_started.md
sed -i 's/\.\/docs\/source\///' ../../docs/source/getting_started.md
cd ../../docs
make html

if [[ "$?" != 0 ]]; then
    echo "HTML build failed" 1>&2
    cd "$cur_dir"
    exit 1
fi

echo "Press <ENTER> to continue..."
read var

make latexpdf
if [[ "$?" != 0 ]]; then
    echo "PDF build failed..." 1>&2
    # cd "$cur_dir"
    # exit 1
fi

echo "copying pdf..."
cp build/latex/<PACKAGE_NAME>.pdf ../resources/pdf/
cd "$cur_dir"

echo "*** END ***"
