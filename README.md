# Getting Started with <epigenetics_db>
__version__: `0.1.0a1`

The `epigenetics_db` package is a toolkit built to house an updatable repository for all types of epigenetic data so that one can provide regulatory context of the loci in a given analysis.

## Installation instructions
```
git clone git@gitlab.com:chrisclarkson/epigenetics_db.git
cd epigenetics_db

python -m pip install --upgrade -r requirements.txt
python -m pip install -e .
```
ddfadsfasfd
### Database design
![figure](./images/epigenetics_db_schema.png?raw-true")

# Using the database
NOTE: I have ordered the instructions for database use as:
* Querying
* Editing
* Creating/adding new data to the database
j
The above are ordered in a way that encourages people to simply use the database that is already created on myriad: `/lustre/projects/DTAdb/resources/epigenetics/sqlite_db/epigenetics.db`. This database is ~12GB and contains data from a variety of sources- see [here](https://gitlab.com/chrisclarkson/epigenetics_repo). So you can query it with your ownd data (the option that specifies which database to query is preset to `/lustre/projects/DTAdb/resources/epigenetics/sqlite_db/epigenetics.db`).

### For querying the database
* `epigenetics_db --action 'query'` -

I would like to retrieve data from my database to relate to analyses that I have done. An example analysis is a series of MRs that I have done contained in the file `results_positive_controls.csv`. I would like to know what states the variants used in these analyses overlap with. NOTE- my input file looks as follows:
```
gene beta SE Variants outcome action_type expression expected mr_number 
NISCH 0.244144379046006 0.0614138999945147 3_52407412_A_G 3_52814709_A_C Diastolic_blood_pressure AGONIST 68.8385 0 1 
NISCH 0.691194647546075 0.292779489971277 3_52088651_A_G 3_52513940_C_T Diastolic_blood_pressure AGONIST 68.8385 0 2 
...
KEAP1 0.22583367696535 0.0732225232481511 19_10028945_C_T Multiple_Sclerosis INHIBITOR 38.1644 -1 13 
```
So I want to query the database and find out what epigenetic marks the `Variants` used in each MR overlap with:
```
epigenetics_db --action 'query' --database epigenetics.db --file ./positive_controls/results_positive_controls.csv \
--delimiter ',' --lift_from GRCh37 --lift_to GRCh38 --liftover True \ --file_type list_of_uni_ids \
--variants_col 'Variants' --header True --output_dir 'results/' \
--other_cols_oi 'mr_number' 'action_type' 'Tissue' 'gene' 'expression' 'outcome'
```
Above the "`--action query`" instructs to search the database `epigenetics.db` based on the variants in "`--file results_positive_controls.csv`" (contained the column "`Variants`" in the file). 


#### Accepted input types
The format of the query input is specified `--file_type list_of_uni_ids` (this can also be a `summary_stat`/ `bed` file). If the loci coordinates are in separate columns, one must also specify the names of the columns containing the chromosome, start and end position in the `--file`. This is done using the `--chr_col`,`--start_col`and `--end_col` options.

#### Liftover options when querying
The variants need to be lifted over to `GRCh38`. You can have the output results lifted back to the original genome version by using the flag `--lift_back True`. 


#### Managing output
The output data will be stored in `--output_dir 'results/'` and will contain the columns `chr_name,start_pos,end_pos,tissue,relevant_gtex_tissue` of the states that overlap the input query and also the columns `'mr_number' 'action_type' 'Tissue' 'gene' 'expression' 'outcome'` will be retained from input file `results_positive_controls.csv`.
#### Quering a specific type of data
If you want to retrieve data of a specific type e.g. `abc_enhancers` data, use the following flags: `--based_on 'metadata_table' --based_on_val 'abc_enhancers'`


### For editing the database
NOTE: For entering data into the database (PLEASE ASK before you want to put new data into /lustre/projects/DTAdb/resources/epigenetics/sqlite_db/epigenetics.db)
* `epigenetics_db --action 'edit'` 

Let's say I made a mistake when making the database and certain fields are not correct.
This type of operation requires a file to tell it what changes to make e.g. 
```
cat epimap_vs_gtex
MUSC.SKELETAL.MUSCLE|Muscle_Skeletal
MUSC.SKELETAL.MUSCLE.CELL|Muscle_Skeletal
NSPH.NEUROSPHERE|
PANC.BODY.PANCREAS|Pancreas
PANC.EMB.ISLT.PREC|Pancreas
```
Now let's change every value where `x=MUSC.SKELETAL.MUSCLE` to `Muscle_Skeletal`
```
epigenetics_db --action 'edit_data' --operation update_column --file epimap_vs_gtex \
--delimiter '|' --table 'files' --columns relevant_gtex_tissue --types STRING --dict_cols 0 1 --based_on tissue
```
Here I'm telling it to perform the "`--operation` `update_column`" on the table "`files`"- specifically the column "`relevant_gtex_tissue`". The changes to be made are supplied by "`--file epimap_vs_gtex`"- changing the value of "`relevant_gtex_tissue`" to `Muscle_Skeletal` when `tissue` is equal to (`--based_on`) `MUSC.SKELETAL.MUSCLE`. The `dict_cols` argument tells which columns in the input file to use when changing values i.e. `--dict_cols 0 1` means based on col 0 in `--file epimap_vs_gtex` change to column 1.


This results in the following SQL operations:
```
UPDATE files SET relevant_gtex_tissue = "Muscle_Skeletal" WHERE tissue = "MUSC.SKELETAL.MUSCLE.CELL"
UPDATE files SET relevant_gtex_tissue = "Muscle_Skeletal" WHERE tissue = "MYO.MYOCYTE"
UPDATE files SET relevant_gtex_tissue = "Muscle_Skeletal" WHERE tissue = "MYO.SKLT.MYOBLAST"
```

### For entering data into the database (PLEASE ASK before you want to put new data into `/lustre/projects/DTAdb/resources/epigenetics/sqlite_db/epigenetics.db`)
* `epigenetics_db --action 'enter_data'` - 

This command let's me both make the database from start and enter new data into it.
e.g.

```
f="Adipose_Adipose_Nuclei_FAT"
epigenetics_db --action 'enter_data' --database epigenetics.db \
--file /lustre/projects/DTAdb/resources/epigenetics/diHMM/data/${f}_hg38.bed.gz \
--tissue ${f} --source "https://github.com/gcyuan/diHMM-cpp" \
--regulatory_category 'ChromatinState' --gtex_relation diHMM_files_GTEx \
--compression 'gzip' --file_type bed --state_col 3
```
Above the "`--operation` `enter_data`" instructs to create (if it doesn't already exist)/enter data to the database `epigenetics.db`. The epigenetic data to be put in the database is in"`--file /lustre/projects/DTAdb/resources/epigenetics/diHMM/data/${f}_hg38.bed.gz`". The format of the query input is specified `--file_type bed` (this can also be a different type of file- if so the columns (column numbers or titles) of the chromosome,start_position and end_position must be provided with `--chr_col`,`--start_col`,`--end_col`). Please specify if the input file has a header with `--header True` and if it is compressed with `--compression`. The regulatory category in question is  `--regulatory_category 'ChromatinState'` and the state of each locus in the input bed file is in column 4 (denoted with `--state_col 3` as the columns are 0 indexed).
Hence the above data will be put into the table `chromatin_state` in the database.