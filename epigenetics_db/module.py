"""
.. include:: ../docs/module.md
"""
from module import __version__, __name__ as pkg_name
from simple_progress import progress
import argparse
import sys
import os
import pprint as pp
import egenetics_db_orm

# Move a load of these to constants
# The prefix for verbose messages and the location to write the messages
MSG_PREFIX = '[info]'
MSG_OUT = sys.stderr


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """
    The main entry point for the script
    """
    # Initialise and parse the command line arguments
    parser = init_cmd_args()
    args = parse_cmd_args(parser)

    # Start a msg output, this will respect the verbosity and will output to
    # STDERR
    m = progress.Msg(prefix='', verbose=args.verbose, file=MSG_OUT)
    m.msg("= Python Package Skeleton ({0} v{1}) =".format(
        pkg_name, __version__))
    m.prefix = MSG_PREFIX
    m.msg_atts(args)

def import_data():
    """
    import sqlalchemy
    """
    pass

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args():
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="Python package skeleton"
    )

    parser.add_argument('infile',
                        type=str,
                        help="A required file")
    parser.add_argument('--opt-file',
                        type=str,
                        help="An optional file")
    parser.add_argument('-v', '--verbose',  action="store_true",
                        help="give more output")

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_cmd_args(parser):
    """
    Initialise the command line arguments and return the parsed arguments

    Parameters
    ----------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    args = parser.parse_args()

    # Required files
    for i in ['infile']:
        setattr(args, i, os.path.expanduser(getattr(args, i)))
        getattr(args, i).open().close()

    # Optional files
    for i in ['opt_file']:
        try:
            setattr(args, i, os.path.expanduser(getattr(args, i)))
            getattr(args, i).open().close()
        except TypeError:
            # Not defined using default
            pass

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def docstring_illustration(arg1, arg2):
    """An example numpy docstring to illustrate the sections.

    Parameters
    ----------
    arg1 : `str`
        A string. This string will be multiplied by the length of the list
    arg2 : `list`
        A list. The list length will be used to multiply the string

    Returns
    -------
    string_by_list : `str`
        Te ``arg1`` argument that .

    Raises
    ------
    IndexError
        If the list or string has no length.

    Notes
    -----
    This function is dumb.

    Examples
    --------
    A simple test:

    >>> docstring_illustration("hello", ['A', 'B'])
    >>> 'hellohello'
    """
    if len(arg1) == 0 or len(arg2) == 0:
        raise IndexError("arguments must have some length")
    return arg1 * len(arg2)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
