#!/usr/bin/env python
from sqlalchemy import (
    create_engine,
    Column,
    ForeignKey,
    Integer,
    Float,
    Text,
    String,
    MetaData
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
import os
from sqlalchemy.sql.sqltypes import Float, Numeric

Base = declarative_base()

class File(Base):
    __tablename__ = 'files'
    file_id = Column(Integer, primary_key=True)
    file_basename = Column(String(255), nullable=False, index=True, unique=True)
    file_path = Column(Text, nullable=False, index=True, unique=False)
    file_type = Column(Text, nullable=False)
    regulation_type = Column(Text, nullable=False)
    tissue=Column(Text, nullable=True)
    relevant_gtex_tissue=Column(Text, nullable=True)
    source_url = Column(Text, nullable=False)
    md5_hash = Column(String(32), nullable=False)
    loci = relationship("Locus", back_populates="files")
    metadata_table=Column(String, nullable=True)

class Locus(Base):
    __tablename__ = 'loci'
    loci_id = Column(Integer, primary_key=True)
    file_id = Column(ForeignKey('files.file_id'))
    chr_name = Column(Integer, nullable=False)
    start_pos = Column(Integer, nullable=False)
    end_pos = Column(Integer, nullable=False)
    files = relationship('File', back_populates="loci")
    transcription_factors = relationship("TranscriptionFactor", back_populates="loci")
    snps_vs_transcription_factors = relationship("SNPs_vs_TranscriptionFactor", back_populates="loci")
    chromatin_state = relationship("ChromatinState", back_populates="loci")
    chip_seq = relationship("ChIPSeq", back_populates="loci")
    atac_seq = relationship("ATACSeq", back_populates="loci")
    enhancers = relationship("Enhancer", back_populates="loci")
    abc_enhancers = relationship("ABC_prediction", back_populates="loci")
    # chromatin_state = relationship("ChromatinState", back_populates="loci")
    def __repr__(self):
        return "<{0}({1},{2},{3},{4})>".format(
            self.__class__.__name__, self.chr_name, self.start_pos,
            self.end_pos, self.metadata_table
        )

class TranscriptionFactor(Base):
    __tablename__ = 'transcription_factors'
    transcription_factor_id = Column(Integer, primary_key=True)
    loci_id = Column(Integer, ForeignKey('loci.loci_id'))
    info = Column(String(50))
    transcription_factor=Column(String(10))
    loci = relationship("Locus", back_populates="transcription_factors")

class SNPs_vs_TranscriptionFactor(Base):
    __tablename__ = 'snps_vs_transcription_factors'
    snp_transcription_factor_id = Column(Integer, primary_key=True)
    loci_id = Column(Integer, ForeignKey('loci.loci_id'))
    transcription_factor=Column(String(10))
    rsid=Column(String(20))
    pval_effect=Column(Float,nullable=True)
    loci = relationship("Locus", back_populates="snps_vs_transcription_factors")

class ChromatinState(Base):
    __tablename__ = 'chromatin_state'
    chromatin_state_id = Column(Integer, primary_key=True)
    loci_id = Column(Integer, ForeignKey('loci.loci_id'))
    state = Column(String(50))
    loci = relationship("Locus", back_populates="chromatin_state")

class ChIPSeq(Base):
    __tablename__ = 'chip_seq'
    peak_id = Column(Integer, primary_key=True)
    loci_id = Column(Integer, ForeignKey('loci.loci_id'))
    peak_height = Column(Float,nullable=True)
    loci = relationship("Locus", back_populates="chip_seq")

class ATACSeq(Base):
    __tablename__ = 'atac_seq'
    chromatin_state_id = Column(Integer, primary_key=True)
    loci_id = Column(Integer, ForeignKey('loci.loci_id'))
    accessibility = Column(Float,nullable=True)
    cluster=Column(String,nullable=True)
    gtex_relation=Column(String,nullable=True)
    loci = relationship("Locus", back_populates="atac_seq")

class Enhancer(Base):
    __tablename__ = 'enhancers'
    chromatin_state_id = Column(Integer, primary_key=True)
    loci_id = Column(Integer, ForeignKey('loci.loci_id'))
    gene=Column(String,nullable=True)
    target_gene = Column(Float,nullable=True)
    loci = relationship("Locus", back_populates="enhancers")

class ABC_prediction(Base):
    __tablename__ = 'abc_enhancers'
    chromatin_state_id = Column(Integer, primary_key=True)
    loci_id = Column(Integer, ForeignKey('loci.loci_id'))
    gene=Column(String,nullable=True)
    target_gene = Column(String,nullable=True)
    probability = Column(Float,nullable=True)
    distance=Column(Float,nullable=True)
    loci = relationship("Locus", back_populates="abc_enhancers")




