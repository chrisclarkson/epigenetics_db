#!/usr/bin/env python
"""
Enter data into the epigenetics database
"""

from sqlalchemy.engine import url
from sqlalchemy.orm import sessionmaker
from sqlalchemy import engine_from_config
from sqlalchemy import create_engine
import random
import argparse
import os
import sys
import epigenetics_db_orm as ep
from epigenetics_db_orm import Base, File, Locus, ChromatinState, ChIPSeq, TranscriptionFactor, ATACSeq, SNPs_vs_TranscriptionFactor
import pandas as pd
import inspect
import json

from file_parser_funcs import *
def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

parser = argparse.ArgumentParser(
    description="enter data into your epigenetics database"
)
parser.add_argument(
    '--file', type=str,
    help="file from which to take data",
    default='/lustre/projects/DTAdb/resources/epigenetics/EPIMAP/URNY.UROTHELIUM.CELL_18_CALLS_segments.bed.gz'
)
parser.add_argument(
    '--database', type=str,
    help="database",
    default='/lustre/projects/DTAdb/resources/epigenetics/sqlite_db/epigenetics.db'
)
parser.add_argument('--regulatory_category',
    type=str,
    help='What type of regulation does this epigenetic data involve?',
    default='active_mark'
)
parser.add_argument(
    '--file_type', 
    type=str,
    help="what type of input file is it e.g. bed",
    default='bed'
)
parser.add_argument('--header',
    type=str2bool,
    help='column containing chromosome',
    default=False
)
parser.add_argument('--compression',
    type=str,
    help='column containing chromosome',
    default=None
)
parser.add_argument('--chrpos_spec',
    nargs='+',
    help='column to parse chrpos data from- specify column and separators e.g. to say first column split on a colon followed by a hyphen do "0 : -" ',
    default=None
)
parser.add_argument('--chr_col',
    type=str,
    help='column containing chromosome',
    default='0'
)
parser.add_argument('--start_col',
    type=str,
    help='column containing start position',
    default='1'
)
parser.add_argument('--end_col',
    type=str,
    help='column containing end position',
    default='2'
)
parser.add_argument('--other_cols_of_interest',
    type=int,
    help='What other columns should be included to the database?',
    default=None
)
parser.add_argument('--assay',
    type=str,
    help='What assay?',
    default='ChiP-seq'
)
parser.add_argument('--state_dict',
    type=str,
    help='json file to provide state dictionary?',
    default=None
)
parser.add_argument('--gtex_relation',
    type=str,
    help='file with translations from tissue to GTEx tissue (read as dataframe)',
    default=None
)
parser.add_argument('--test_header',
    type=bool,
    help='test with just the head of the file?',
    default=False
)
parser.add_argument('--tissue',
    type=str,
    help='What tissue was experiment performed in?',
    default=None
)
parser.add_argument('--sep',
    type=str,
    help='tab separated or space?',
    default='\t'
)
parser.add_argument('--md5_hash',
    type=str,
    help='is this an md5 hash file',
    default="ac3ac2a1fd35e87cbb5ef32260cd2768"
)
parser.add_argument('--source',
    type=str,
    help='URL source of file',
    default='epimap'
)
parser.add_argument('--state_col',
    type=int,
    help='state col?',
    default=None
)
parser.add_argument('--peak_height_col',
    type=int,
    help='peak height col?',
    default=None
)
parser.add_argument('--rsid_col',
    type=int,
    help='rsid col?',
    default=None
)
parser.add_argument('--tf_col',
    type=int,
    help='TF col?',
    default=None
)
parser.add_argument('--pval_col',
    type=int,
    help='Pval col?',
    default=None
)
parser.add_argument('--peak_heights_col',
    type=int,
    help='peak height col?',
    default=None
)
parser.add_argument('--cluster_col',
    type=int,
    help='cluster col?',
    default=None
)
parser.add_argument('--chunk_size',
    type=int,
    help='chunk_size?',
    default=1000
)


# standard query: select * from loci join files_table on loci.id=files_table.id join tf on files_table.id=tf.file_id limit 10;
args = parser.parse_args()

print(args.chr_col)
try:
    int(args.chr_col)
    args.chr_col=int(args.chr_col)
except:
    pass

try:
    int(args.start_col)
    args.start_col=int(args.start_col)
except:
    pass

try:
    int(args.end_col)
    args.end_col=int(args.end_col)
except:
    pass

db_name=args.database
engine = create_engine('sqlite:///{0}'.format(db_name))
Session = sessionmaker(bind=engine)
Base.metadata.create_all(engine,checkfirst=True)
session = Session()


if args.header:
    header=True
else:
    header=None

if args.compression is not None:
    if header:
        df=pd.read_csv(args.file,sep=args.sep,compression=args.compression)
    else:
        df=pd.read_csv(args.file,sep=args.sep,header=header,compression=args.compression)
else:
    if header:
        df=pd.read_csv(args.file,sep=args.sep)
    else:
        df=pd.read_csv(args.file,sep=args.sep,header=header)

# if args.test_header:
#     df=df.head()

if args.chrpos_spec is not None:
    if len(args.chrpos_spec)==2:
        col,delim=args.chrpos_spec[0],args.chrpos_spec[1]
        if len(df[col].values[0].split(delim))==3:
            df[['chr_name','start_col','end_col']]=df[col].str.split(delim,expand=True)
        elif len(df[col].values[0].split(delim))==2:
            df[['chr_name','start_col']]=df[col].str.split(delim,expand=True)
            df['end_col']=df['start_col']
    elif len(args.chrpos_spec)==3:
        col,chr_delim,startend_delim=args.chrpos_spec[0],args.chrpos_spec[1],args.chrpos_spec[2]
        col=df.columns[int(col)]
        df[['chr_name','start_end']]=df[col].str.split(chr_delim,expand=True)
        df[['start_col','end_col']]=df['start_end'].str.split(startend_delim,expand=True)
    df['chr_name']=df['chr_name'].str.replace('chr','')
    chr_col,start_col,end_col=df.columns.get_loc('chr_name'),df.columns.get_loc('start_end'),df.columns.get_loc('end_col')
else:
    chr_col1,start_col1,end_col1=df.columns[args.chr_col],df.columns[args.start_col],df.columns[args.end_col]
    if 'chr' in str(df[chr_col1].values[0]):
        df[chr_col1]=df[chr_col1].str.replace('chr','')

if args.gtex_relation is not None:
    gtex_dict=pd.read_csv(args.gtex_relation,sep=r'\s+',header=None)
else:
    gtex_dict=None

print(df)

tables_dict={
    'ChromatinState':{
        'metadata_table':'chromatin_state'
    },
    'ATACSeq':{
        'metadata_table':'atac_seq'
    },
    'ChIPSeq':{
        'metadata_table':'chip_seq'
    },
    'SNPs_vs_TranscriptionFactor':{
        'metadata_table':'snps_vs_transcription_factors'
    }
}

if gtex_dict is not None and args.tissue is not None:
    gtex_tissue=gtex_dict.loc[gtex_dict[0]==args.tissue,1].values[0]
    print(gtex_tissue)
    if pd.isna(gtex_tissue):
        gtex_tissue=None
    file = File(
        file_basename=os.path.basename(args.file),
        file_path=os.path.dirname(args.file),
        source_url=args.source,
        regulation_type=args.regulatory_category,
        relevant_gtex_tissue=gtex_tissue,
        file_type=args.file_type,
        tissue=args.tissue,
        md5_hash="ac3ac2a1fd35e87cbb5ef32260cd2768",
        metadata_table=tables_dict[args.regulatory_category]['metadata_table']
    )
else:
    file = File(
        file_basename=os.path.basename(args.file),
        file_path=os.path.dirname(args.file),
        source_url=args.source,
        regulation_type=args.regulatory_category,
        file_type=args.file_type,
        tissue=args.tissue,
        md5_hash="ac3ac2a1fd35e87cbb5ef32260cd2768",
        metadata_table=tables_dict[args.regulatory_category]['metadata_table']
    )


args_dict={
    'ChromatinState':{
        'tf_col':args.tf_col,'chr_col':args.chr_col,
        'start_col':args.start_col,'end_col':args.end_col,'states_dict':args.state_dict,
        'state_col':args.state_col
    },
    'ATACSeq':{
        'chr_col':args.chr_col,
        'start_col':args.start_col,'end_col':args.end_col,
        'cluster_col':args.cluster_col,'states_dict':args.state_dict
    },
    'ChIPSeq':{
        'chr_col':args.chr_col,
        'start_col':args.start_col,'end_col':args.end_col,
        'peak_heights_col':args.peak_heights_col
    },
    'SNPs_vs_TranscriptionFactor':{
        'chr_col':args.chr_col,
        'start_col':args.start_col,'end_col':args.end_col,
        'tf_col':args.tf_col,'rsid_col':args.rsid_col,'pval_col':args.pval_col
    }
}

func_dict={
    'ChromatinState':add_chromatin_state_data,
    'ATACSeq':add_atac_seq_data,
    'ChIPSeq':add_chipseq_peaks_data,
    'SNPs_vs_TranscriptionFactor':add_snp_tf_data
}

n=args.chunk_size
df = [df[i:i+n] for i in range(0,df.shape[0],n)]

for i in df:
    if args.test_header:
        i=i.head()
    file=func_dict[args.regulatory_category](i,file,**args_dict[args.regulatory_category])
    session.add(file)
    session.commit()

session.close()
