from epigenetics_db_orm import Base, File, Locus, ChromatinState, ChIPSeq, TranscriptionFactor, ATACSeq, SNPs_vs_TranscriptionFactor
import json
import pandas as pd

def add_chromatin_state_data(df,file,tf_col,chr_col,start_col,end_col,states_dict,state_col,**args_dict):
    if states_dict is not None:
        with open(states_dict) as jsonfile:
            state_dict = json.load(jsonfile)
        for row in df.itertuples():
            locus = Locus(
                chr_name=row[chr_col+1],
                start_pos=row[start_col+1],
                end_pos=row[end_col+1],
                files=file
            )
            table_row = ChromatinState(
                state=state_dict[state_col+1], loci=locus
            )
    else:
        for row in df.itertuples():
            locus = Locus(
                chr_name=row[chr_col+1],
                start_pos=row[start_col+1],
                end_pos=row[end_col+1],
                files=file
            )
            table_row = ChromatinState(
                state=row[state_col+1], loci=locus
            )
    return file


def add_atac_seq_data(df,file,chr_col,start_col,end_col,cluster_col,states_dict,**args_dict):
    # df[chr_col]=df[chr_col].str.replace('chr','')
    if states_dict is not None:
        with open(states_dict) as jsonfile:
            state_dict = json.load(jsonfile)
        for row in df.itertuples():
            locus = Locus(
                chr_name=row[chr_col+1],
                start_pos=row[start_col+1],
                end_pos=row[end_col+1],
                files=file
            )
            table_row = ATACSeq(
                accessibility=1,
                cluster=state_dict[str(row[cluster_col+1])], loci=locus
            )
    else:
        for row in df.itertuples():
            locus = Locus(
                chr_name=row[chr_col+1].replace('chr',''),
                start_pos=row[start_col+1],
                end_pos=row[end_col+1],
                files=file
            )
            table_row = ATACSeq(
                accessibility=1,
                cluster=row[cluster_col+1], loci=locus
            )
    return file


def add_snp_tf_data(df,file,chr_col,start_col,end_col,tf_col,rsid_col,pval_col,**args_dict):
    # df[chr_col]=df[chr_col].str.replace('chr','')
    for row in df.itertuples():
        locus = Locus(
            chr_name=row[chr_col+1],
            start_pos=row[start_col+1],
            end_pos=row[end_col+1],
            files=file
        )
        table_row = SNPs_vs_TranscriptionFactor(
            transcription_factor=row[tf_col+1],
            rsid=row[rsid_col+1],
            pval_effect=row[pval_col+1],
            loci=locus
        )
    return file


def add_chipseq_peaks_data(df,file,chr_col,start_col,end_col,peak_heights_col,**args_dict):
    if peak_heights_col is not None:
        for row in df.itertuples():
            locus = Locus(
                chr_name=row[chr_col+1],
                start_pos=row[start_col+1],
                end_pos=row[end_col+1],
                files=file
            )
            table_row = ChIPSeq(
                peak_height=row[peak_heights_col+1], loci=locus
            )
    else:
        for row in df.itertuples():
            locus = Locus(
                chr_name=row[chr_col+1],
                start_pos=row[start_col+1],
                end_pos=row[end_col+1],
                files=file
            )
    return file


def parse_bed(file,sep,header,compression,**parse_args_dict):
    if compression is not None:
        if header:
            df=pd.read_csv(file,sep=sep,compression=compression)
        else:
            df=pd.read_csv(file,sep=sep,header=header,compression=compression)
    else:
        if header:
            df=pd.read_csv(file,sep=sep)
        else:
            df=pd.read_csv(file,sep=sep,header=header)
    return [df]

def parse_vcf(file):
    pass

def parse_sumstats(file,sep,header,compression,**parse_args_dict):
    if compression is not None:
        if header:
            df=pd.read_csv(file,sep=sep,compression=compression)
        else:
            df=pd.read_csv(file,sep=sep,header=header,compression=compression)
    else:
        if header:
            df=pd.read_csv(file,sep=sep)
        else:
            df=pd.read_csv(file,sep=sep,header=header)
    return [df]

def parse_uni_ids(file,sep,header,compression,lift,lift_from,lift_to,variants_col,**parse_args_dict):
    if compression is not None:
        if header:
            df=pd.read_csv(file,sep=sep,compression=compression)
        else:
            df=pd.read_csv(file,sep=sep,header=header,compression=compression)
    else:
        if header:
            df=pd.read_csv(file,sep=sep)
        else:
            df=pd.read_csv(file,sep=sep,header=header)
    from merit.utils import liftover
    from merit import dataset
    variant_list=df[variants_col]
    for var in variant_list:
        variants=pd.DataFrame(var.split(' '))
        variants.index=variants[0]
        variants.index.name='uni_id'
        variants[['chr_name','start_pos','other_allele','effect_allele']]=variants[0].str.split('_',expand=True)
        variants['end_pos']=variants['start_pos']
        variants=variants.iloc[:,1:]
        variants['effect_size']=0.5
        variants['se']=0.5
        if lift:
            variants=dataset.set_gwas(variants,effect_type='beta')
            variants, coords = liftover.liftover_uni_ids(
                        variants,
                        source_assembly=lift_from,
                        target_assembly=lift_to,
                        method='pyliftover'
                    )
            variants['start_pos'],variants['end_pos']=coords['start_pos'],coords['end_pos']
        yield variants

