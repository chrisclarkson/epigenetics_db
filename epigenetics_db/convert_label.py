import argparse
import pandas as pd
parser = argparse.ArgumentParser(
    description="enter data into your epigenetics database"
)
parser.add_argument(
    '--infile', type=str,
    help="file from which to take data",
    default='CVD_C-reactive_protein_chromatin_state_metadata.csv'
)
parser.add_argument(
    '--outfile', type=str,
    help="file from which to take data",
    default='CVD_HDL_cholesterol_abc_enhancers_metadata.txt'
)
parser.add_argument('--chr_col',
    type=str,
    help='column containing chromosome',
    default='chr_name'
)
parser.add_argument('--start_col',
    type=str,
    help='column containing start position',
    default='start_pos'
)
parser.add_argument('--pval_col',
    type=str,
    help='column containing start position',
    default='pval'
)
parser.add_argument('--label_cols',
    type=str,
    nargs='+',
    help='column containing end position'
)
parser.add_argument('--locus_size',
    type=int,
    help='column containing end position',
    default=10000
)
parser.add_argument('--include_tissues',
    type=str,
    nargs='+',
    help='column containing end position'
)
args = parser.parse_args()

cols={
    "chr":args.chr_col,
    "pos":args.start_col,
    "pval":args.pval_col,
    "peak.col":None,
    "poi.col":None
}

data=pd.read_csv(args.infile,sep='\t')

data2=pd.DataFrame()
for key in cols.keys():
    if cols[key] is not None:
        data2[key]=data[cols[key]]
    else:
        data2[key]=1000

data2["use.locus.size"]=args.locus_size
data2['label.col']=data2.shape[1]
data2['label.name']=''
for col in args.label_cols:
    data2['label.name']=data2['label.name']+'_'+data[col]

data2['label.name']=data2['label.name'].str.replace('^_','')
data2=data2.copy().dropna()
data3=data2[['chr','pos','pval','peak.col','poi.col','label.col','label.name','use.locus.size']]
data3['label.col']=data3.columns.get_loc('label.name')
# print(data3)
if args.include_tissues is not None:
    # data3=data3.loc[data['relevant_gtex_tissue'].isin(args.include_tissues)]
    mask = data.iloc[:, data.columns.get_loc('relevant_gtex_tissue')].str.contains(r'\b(?:{})\b'.format('|'.join(args.include_tissues)))
    # print(mask)
    mask[pd.isna(mask)]=False
    # print(mask)
    data3 = data3[mask]

# print(data3)
data3.to_csv(args.outfile,sep='\t',index=False,na_rep="NA")
