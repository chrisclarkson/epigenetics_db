#!/usr/bin/env python
"""
Enter data into the epigenetics database
"""

from sqlalchemy.engine import url
from sqlalchemy.orm import sessionmaker
from sqlalchemy import engine_from_config
from sqlalchemy import create_engine
import random
import argparse
import os
import sys
import epigenetics_db_orm as ep
from epigenetics_db_orm import Base, File, Locus, ChromatinState, TranscriptionFactor, ATACSeq
import pandas as pd
import inspect

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

parser = argparse.ArgumentParser(
    description="edit data in your epigenetics database"
)
parser.add_argument('--database',
    type=str,
    help="database",
    default='/lustre/projects/DTAdb/resources/epigenetics/sqlite_db/epigenetics.db'
)
parser.add_argument('--operation',
    type=str,
    help="edit/add column/delete table etc.",
    default=None
)
parser.add_argument('--table',
    type=str,
    help='Table to be handled',
    default=None
)
parser.add_argument('--based_on',
    type=str,
    help='change colx based on coly/delete row based on colx'
)
parser.add_argument('--based_on_val',
    type=str,
    help='delete row x based on val x (column specified by --based_on)'
)
parser.add_argument('--columns',
    type=str,
    nargs='+',
    help='columns to be to be made'
)
parser.add_argument('--types',
    type=str,
    nargs='+',
    help='Types of columns to be handled'
)
parser.add_argument('--dict_cols',
    type=str,
    nargs='+',
    help='The columns to use when modifying column based on --file in at row i replace with j'
)
parser.add_argument('--file',
    type=str,
    help='File to use as update reference'
)
parser.add_argument('--header',
    type=str,
    help='Header in reference file?',
    default=None
)
parser.add_argument('--add_col',
    type=str,
    help='Add new column to be edited?',
    default=None
)
parser.add_argument('--delimiter',
    type=str,
    help='tab separated or space?',
    default='\t'
)

args = parser.parse_args()
args = parser.parse_args()
db_name=args.database
import sqlite3
conn = sqlite3.connect(db_name)
cursor = conn.cursor()

def delete_table(table,**args_dict):
    cursor.execute("DROP TABLE IF EXISTS {0}".format(table))

def delete_file(col_oi,val,conn,cursor,**args_dict):
    try:
        int(val)
        val=int(val)
        is_int=True
    except:
        is_int=False
    if is_int:
        query="select * from files where {0} = {1}".format(col_oi,val)
    else:
        query="select * from files where {0} = '{1}'".format(col_oi,val)
    result=pd.read_sql(query, conn)
    file_id=result['file_id'].values[0]
    metadata_table=result['metadata_table'].values[0]
    print(file_id)
    print(metadata_table)
    query="select * from loci where file_id = {0}".format(file_id)
    loci_ids=pd.read_sql(query, conn)['loci_id']
    print(loci_ids)
    cmd="delete from files where file_id = {0}".format(str(file_id))
    cursor.execute(cmd)
    cmd="delete from {0} where loci_id in {1}".format(metadata_table,str(tuple(loci_ids)))
    cursor.execute(cmd)
    cmd="delete from files where {0} = '{1}'".format(col_oi,val)
    cursor.execute(cmd)


def create_table(table,columns,types,**args_dict):
    cmd='CREATE TABLE '+table+' '
    params=[]
    for i in range(len(columns)):
        if i==0:
            types[i]=types[i]+' PRIMARY KEY'
        column=columns[i]+' '+types[i]
        params.append(column)
    params=', '.join(params)
    cmd=cmd+params
    print(cmd)
    cursor.execute(cmd)

def add_column(table,columns,types,**args_dict):
    for i in range(len(columns)):
        cursor.execute("ALTER TABLE {0} ADD COLUMN {1} {2}".format(table,columns[i],types[i]))

def update_column(table,column,based_on,type,add_col,file,col1,col2,sep,header,**args_dict):
    if header:
        df=pd.read_csv(file,sep=sep,header=None)
    else:
        df=pd.read_csv(file,sep=sep,header=header)
    print(add_col)
    if add_col is not None:
        cursor.execute("ALTER TABLE {0} ADD COLUMN {1} {2}".format(table,column,type))
    for i,j in zip(df[col1],df[col2]):
        cmd='UPDATE {0} SET {1} = "{3}" WHERE {4} = "{2}"'.format(table,column,i,j,based_on)
        print(cmd)
        cursor.execute(cmd)

try:
    int(args.dict_cols[0])
    args.dict_cols[0]=int(args.dict_cols[0])
except:
    pass

try:
    int(args.dict_cols[1])
    args.dict_cols[1]=int(args.dict_cols[1])
except:
    pass

func_dict={
    'create_table':create_table,
    'add_column':add_column,
    'delete_table':delete_table,
    'update_column':update_column,
    'delete_file':delete_file
}

if args.columns is None:
    args.columns=[None]

if args.types is None:
    args.types=[None]

if args.dict_cols is None:
    args.dict_cols=[-1,-1]


args_dict={
    'update_column':{
        'table':args.table,'column':args.columns[0],'based_on':args.based_on,'type':args.types[0],'add_col':args.add_col,
        'file':args.file,'col1':int(args.dict_cols[0]),'col2':int(args.dict_cols[1]),'sep':args.delimiter,
        'header':args.header
    },
    'create_table':{
        'table':args.table,'columns':args.columns,'types':args.types
    },
    'add_column':{
        'table':args.table,'columns':args.columns,'types':args.types
    },
    'delete_table':{
        'table':args.table
    },
    'delete_file':{
        'col_oi':args.based_on,'val':args.based_on_val,'conn':conn,'cursor':cursor
    }
}

if args.table is not None or args.operation=='delete_file':
    if args.operation is not None:
        func_dict[args.operation](**args_dict[args.operation])
    else:
        print('no operation specified')
else:
    print('no table specified')

conn.commit()
conn.close()

