#!/usr/bin/env python
"""
Enter data into the epigenetics database
"""

from sqlalchemy.engine import url
from sqlalchemy.orm import sessionmaker
from sqlalchemy import engine_from_config
from sqlalchemy import create_engine
import random
import argparse
import os
import sys

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


parser = argparse.ArgumentParser(
    description="enter data into your epigenetics database"
)
parser.add_argument(
    '--action', type=str,
    help="enter action to perform: edit/query/enter data to database",
    default=None
)
parser.add_argument(
    '--file', type=str,
    help="file from which to take data",
    default='/lustre/projects/DTAdb/resources/epigenetics/EPIMAP/URNY.UROTHELIUM.CELL_18_CALLS_segments.bed.gz'
)
parser.add_argument(
    '--database', type=str,
    help="database",
    default='/lustre/projects/DTAdb/resources/epigenetics/sqlite_db/epigenetics.db'
)
parser.add_argument('--regulatory_category',
    type=str,
    help='What type of regulation does this epigenetic data involve?',
    default='active_mark'
)
parser.add_argument(
    '--file_type', 
    type=str,
    help="what type of input file is it e.g. bed",
    default='bed'
)
parser.add_argument('--header',
    type=str2bool,
    help='column containing chromosome',
    default=False
)
parser.add_argument('--compression',
    type=str,
    help='column containing chromosome',
    default=None
)
parser.add_argument('--chrpos_spec',
    nargs='+',
    help='column to parse chrpos data from- specify column and separators e.g. to say first column split on a colon followed by a hyphen do "0 : -" ',
    default=None
)
parser.add_argument('--chr_col',
    type=str,
    help='column containing chromosome',
    default='0'
)
parser.add_argument('--start_col',
    type=str,
    help='column containing start position',
    default='1'
)
parser.add_argument('--end_col',
    type=str,
    help='column containing end position',
    default='2'
)
parser.add_argument('--other_cols_of_interest',
    type=int,
    help='What other columns should be included to the database?',
    default=None
)
parser.add_argument('--assay',
    type=str,
    help='What assay?',
    default='ChiP-seq'
)
parser.add_argument('--state_dict',
    type=str,
    help='json file to provide state dictionary?',
    default=None
)
parser.add_argument('--gtex_relation',
    type=str,
    help='file with translations from tissue to GTEx tissue (read as dataframe)',
    default=None
)
parser.add_argument('--test_header',
    type=bool,
    help='test with just the head of the file?',
    default=False
)
parser.add_argument('--tissue',
    type=str,
    help='What tissue was experiment performed in?',
    default=None
)

parser.add_argument('--md5_hash',
    type=str,
    help='is this an md5 hash file',
    default="ac3ac2a1fd35e87cbb5ef32260cd2768"
)
parser.add_argument('--source',
    type=str,
    help='URL source of file',
    default='epimap'
)
parser.add_argument('--state_col',
    type=int,
    help='state col?',
    default=None
)
parser.add_argument('--peak_height_col',
    type=int,
    help='peak height col?',
    default=None
)
parser.add_argument('--rsid_col',
    type=int,
    help='rsid col?',
    default=None
)
parser.add_argument('--tf_col',
    type=int,
    help='TF col?',
    default=None
)
parser.add_argument('--pval_col',
    type=int,
    help='Pval col?',
    default=None
)
parser.add_argument('--peak_heights_col',
    type=int,
    help='peak height col?',
    default=None
)
parser.add_argument('--cluster_col',
    type=int,
    help='cluster col?',
    default=None
)
parser.add_argument('--chunk_size',
    type=int,
    help='chunk_size?',
    default=1000
)
parser.add_argument('--delimiter',
    type=str,
    help='tab separated or space?',
    default='\t'
)
parser.add_argument('--liftover',
    type=str2bool,
    help='liftover ',
    default=False
)
parser.add_argument('--lift_from',
    type=str,
    help='liftover from what genome version e.g. GRCh37',
)
parser.add_argument('--lift_to',
    type=str,
    help='liftover from what genome version e.g. GRCh38'
)
parser.add_argument('--variants_col',
    type=str,
    help='column with uni_ids in it',
    default='Variants'
)
parser.add_argument('--other_cols_oi',
    nargs='+',
    type=str,
    help='column with uni_ids in it',
    default=None
)
parser.add_argument('--operation',
    type=str,
    help="edit/add column/delete table etc.",
    default=None
)
parser.add_argument('--table',
    type=str,
    help='Table to be handled',
    default=None
)
parser.add_argument('--based_on',
    type=str,
    help='change colx based on coly/delete row based on colx'
)
parser.add_argument('--based_on_val',
    type=str,
    help='delete row x based on val x (column specified by --based_on)'
)
parser.add_argument('--columns',
    type=str,
    nargs='+',
    help='columns to be to be made'
)
parser.add_argument('--types',
    type=str,
    nargs='+',
    help='Types of columns to be handled'
)
parser.add_argument('--dict_cols',
    type=str,
    nargs='+',
    help='The columns to use when modifying column based on --file in at row i replace with j'
)
parser.add_argument('--add_col',
    type=str,
    help='Add new column to be edited?',
    default=None
)
parser.add_argument('--output_dir',
    type=str,
    help='Dir to output query results to',
    default=None
)
# standard query: select * from loci join files_table on loci.id=files_table.id join tf on files_table.id=tf.file_id limit 10;

def main():
    args = parser.parse_args()
    if args.action is None:
        print('You must provide an action to perform e.g. enter data/edit/query database')
    elif args.action=='enter_data':
        # import epigenetics_db_orm as ep
        from .epigenetics_db_orm import Base, File, Locus, ChromatinState, ChIPSeq, TranscriptionFactor, ATACSeq, SNPs_vs_TranscriptionFactor
        import pandas as pd
        import inspect
        import json
        from .file_parser_funcs import add_chromatin_state_data, add_atac_seq_data, add_snp_tf_data, add_chipseq_peaks_data

        try:
            int(args.chr_col)
            args.chr_col=int(args.chr_col)
        except:
            pass

        try:
            int(args.start_col)
            args.start_col=int(args.start_col)
        except:
            pass

        try:
            int(args.end_col)
            args.end_col=int(args.end_col)
        except:
            pass

        db_name=args.database
        engine = create_engine('sqlite:///{0}'.format(db_name))
        Session = sessionmaker(bind=engine)
        Base.metadata.create_all(engine,checkfirst=True)
        session = Session()
        if args.header:
            header=True
        else:
            header=None

        if args.compression is not None:
            if header:
                df=pd.read_csv(args.file,sep=args.delimiter,compression=args.compression)
            else:
                df=pd.read_csv(args.file,sep=args.delimiter,header=header,compression=args.compression)
        else:
            if header:
                df=pd.read_csv(args.file,sep=args.delimiter)
            else:
                df=pd.read_csv(args.file,sep=args.delimiter,header=header)

        if args.chrpos_spec is not None:
            if len(args.chrpos_spec)==2:
                col,delim=args.chrpos_spec[0],args.chrpos_spec[1]
                if len(df[col].values[0].split(delim))==3:
                    df[['chr_name','start_col','end_col']]=df[col].str.split(delim,expand=True)
                elif len(df[col].values[0].split(delim))==2:
                    df[['chr_name','start_col']]=df[col].str.split(delim,expand=True)
                    df['end_col']=df['start_col']
            elif len(args.chrpos_spec)==3:
                col,chr_delim,startend_delim=args.chrpos_spec[0],args.chrpos_spec[1],args.chrpos_spec[2]
                col=df.columns[int(col)]
                df[['chr_name','start_end']]=df[col].str.split(chr_delim,expand=True)
                df[['start_col','end_col']]=df['start_end'].str.split(startend_delim,expand=True)
            df['chr_name']=df['chr_name'].str.replace('chr','')
            chr_col,start_col,end_col=df.columns.get_loc('chr_name'),df.columns.get_loc('start_end'),df.columns.get_loc('end_col')
        else:
            chr_col1,start_col1,end_col1=df.columns[args.chr_col],df.columns[args.start_col],df.columns[args.end_col]
            if 'chr' in str(df[chr_col1].values[0]):
                df[chr_col1]=df[chr_col1].str.replace('chr','')

        if args.gtex_relation is not None:
            gtex_dict=pd.read_csv(args.gtex_relation,sep=r'\s+',header=None)
        else:
            gtex_dict=None

        tables_dict={
            'ChromatinState':{
                'metadata_table':'chromatin_state'
            },
            'ATACSeq':{
                'metadata_table':'atac_seq'
            },
            'ChIPSeq':{
                'metadata_table':'chip_seq'
            },
            'SNPs_vs_TranscriptionFactor':{
                'metadata_table':'snps_vs_transcription_factors'
            }
        }

        if gtex_dict is not None and args.tissue is not None:
            gtex_tissue=gtex_dict.loc[gtex_dict[0]==args.tissue,1].values[0]
            print(gtex_tissue)
            if pd.isna(gtex_tissue):
                gtex_tissue=None
            file = File(
                file_basename=os.path.basename(args.file),
                file_path=os.path.dirname(args.file),
                source_url=args.source,
                regulation_type=args.regulatory_category,
                relevant_gtex_tissue=gtex_tissue,
                file_type=args.file_type,
                tissue=args.tissue,
                md5_hash="ac3ac2a1fd35e87cbb5ef32260cd2768",
                metadata_table=tables_dict[args.regulatory_category]['metadata_table']
            )
        else:
            file = File(
                file_basename=os.path.basename(args.file),
                file_path=os.path.dirname(args.file),
                source_url=args.source,
                regulation_type=args.regulatory_category,
                file_type=args.file_type,
                tissue=args.tissue,
                md5_hash="ac3ac2a1fd35e87cbb5ef32260cd2768",
                metadata_table=tables_dict[args.regulatory_category]['metadata_table']
            )

        args_dict={
            'ChromatinState':{
                'tf_col':args.tf_col,'chr_col':args.chr_col,
                'start_col':args.start_col,'end_col':args.end_col,'states_dict':args.state_dict,
                'state_col':args.state_col
            },
            'ATACSeq':{
                'chr_col':args.chr_col,
                'start_col':args.start_col,'end_col':args.end_col,
                'cluster_col':args.cluster_col,'states_dict':args.state_dict
            },
            'ChIPSeq':{
                'chr_col':args.chr_col,
                'start_col':args.start_col,'end_col':args.end_col,
                'peak_heights_col':args.peak_heights_col
            },
            'SNPs_vs_TranscriptionFactor':{
                'chr_col':args.chr_col,
                'start_col':args.start_col,'end_col':args.end_col,
                'tf_col':args.tf_col,'rsid_col':args.rsid_col,'pval_col':args.pval_col
            }
        }

        func_dict={
            'ChromatinState':add_chromatin_state_data,
            'ATACSeq':add_atac_seq_data,
            'ChIPSeq':add_chipseq_peaks_data,
            'SNPs_vs_TranscriptionFactor':add_snp_tf_data
        }

        n=args.chunk_size
        df = [df[i:i+n] for i in range(0,df.shape[0],n)]

        for i in df:
            if args.test_header:
                i=i.head()
            file=func_dict[args.regulatory_category](i,file,**args_dict[args.regulatory_category])
            session.add(file)
            session.commit()
        session.close()

    elif args.action=='edit_data':
        db_name=args.database
        import sqlite3
        conn = sqlite3.connect(db_name)
        cursor = conn.cursor()
        import pandas as pd
        def delete_table(table,**args_dict):
            cursor.execute("DROP TABLE IF EXISTS {0}".format(table))

        def delete_file(col_oi,val,conn,cursor,**args_dict):
            try:
                int(val)
                val=int(val)
                is_int=True
            except:
                is_int=False
            if is_int:
                query="select * from files where {0} = {1}".format(col_oi,val)
            else:
                query="select * from files where {0} = '{1}'".format(col_oi,val)
            result=pd.read_sql(query, conn)
            file_id=result['file_id'].values[0]
            metadata_table=result['metadata_table'].values[0]
            print(file_id)
            print(metadata_table)
            query="select * from loci where file_id = {0}".format(file_id)
            loci_ids=pd.read_sql(query, conn)['loci_id']
            print(loci_ids)
            cmd="delete from files where file_id = {0}".format(str(file_id))
            cursor.execute(cmd)
            cmd="delete from {0} where loci_id in {1}".format(metadata_table,str(tuple(loci_ids)))
            cursor.execute(cmd)
            cmd="delete from files where {0} = '{1}'".format(col_oi,val)
            cursor.execute(cmd)

        def create_table(table,columns,types,**args_dict):
            cmd='CREATE TABLE '+table+' '
            params=[]
            for i in range(len(columns)):
                if i==0:
                    types[i]=types[i]+' PRIMARY KEY'
                column=columns[i]+' '+types[i]
                params.append(column)
            params=', '.join(params)
            cmd=cmd+params
            print(cmd)
            cursor.execute(cmd)

        def add_column(table,columns,types,**args_dict):
            for i in range(len(columns)):
                cursor.execute("ALTER TABLE {0} ADD COLUMN {1} {2}".format(table,columns[i],types[i]))

        def update_column(table,column,based_on,type,add_col,file,col1,col2,sep,header,**args_dict):
            if header:
                df=pd.read_csv(file,sep=sep)
            else:
                df=pd.read_csv(file,sep=sep,header=None)
            print(add_col)
            if add_col is not None:
                cursor.execute("ALTER TABLE {0} ADD COLUMN {1} {2}".format(table,column,type))
            for i,j in zip(df[col1],df[col2]):
                print(i,j)
                cmd='UPDATE {0} SET {1} = "{3}" WHERE {4} = "{2}"'.format(table,column,i,j,based_on)
                print(cmd)
                cursor.execute(cmd)

        def index_table(table,**args_dict):
            cmd='CREATE INDEX pos_idx_{0} ON {0} (loci_id)'.format(table)
            cursor.execute(cmd)

        try:
            int(args.dict_cols[0])
            args.dict_cols[0]=int(args.dict_cols[0])
        except:
            pass

        try:
            int(args.dict_cols[1])
            args.dict_cols[1]=int(args.dict_cols[1])
        except:
            pass

        func_dict={
            'create_table':create_table,
            'add_column':add_column,
            'delete_table':delete_table,
            'update_column':update_column,
            'delete_file':delete_file,
            'index_table':index_table
        }

        if args.columns is None:
            args.columns=[None]

        if args.types is None:
            args.types=[None]

        if args.dict_cols is None:
            args.dict_cols=[-1,-1]

        args_dict={
            'update_column':{
                'table':args.table,'column':args.columns[0],'based_on':args.based_on,'type':args.types[0],'add_col':args.add_col,
                'file':args.file,'col1':int(args.dict_cols[0]),'col2':int(args.dict_cols[1]),'sep':args.delimiter,
                'header':args.header
            },
            'create_table':{
                'table':args.table,'columns':args.columns,'types':args.types
            },
            'add_column':{
                'table':args.table,'columns':args.columns,'types':args.types
            },
            'delete_table':{
                'table':args.table
            },
            'delete_file':{
                'col_oi':args.based_on,'val':args.based_on_val,'conn':conn,'cursor':cursor
            },
            'index_table':{
                'table':args.table
            }
        }

        if args.table is not None or args.operation=='delete_file':
            if args.operation is not None:
                func_dict[args.operation](**args_dict[args.operation])
            else:
                print('no operation specified')
        else:
            print('no table specified')

        conn.commit()
        conn.close()

    elif args.action=='query':
        from .file_parser_funcs import parse_bed, parse_sumstats, parse_uni_ids
        from .query_funcs import query, query_uni_ids, query_uni_ids2,query_metadata
        import os
        import sys
        import pandas as pd
        db_name=args.database
        import sqlite3
        conn = sqlite3.connect(db_name)
        cursor = conn.cursor()
        if args.header:
            header=True
        else:
            header=None

        try:
            int(args.dict_cols[0])
            args.dict_cols[0]=int(args.dict_cols[0])
        except:
            pass

        try:
            int(args.dict_cols[1])
            args.dict_cols[1]=int(args.dict_cols[1])
        except:
            pass

        parse_dict={
            'bed':parse_bed,
            'sumstats':parse_sumstats,
            'list_of_uni_ids': parse_uni_ids
        }

        print(args.file,args.delimiter,args.header,args.compression,args.liftover,args.lift_from,args.lift_to)
        parse_args_dict={
            'bed':{
                'file':args.file,'sep':args.delimiter,'header':args.header,'compression':args.compression
            },
            'sumstats':{
                'file':args.file,'sep':args.delimiter,'header':args.header,'compression':args.compression
            },
            'list_of_uni_ids':{
                'file':args.file,'sep':args.delimiter,'header':args.header,'compression':args.compression,
                'lift':args.liftover,'lift_from':args.lift_from,'lift_to':args.lift_to,'other_cols_oi':args.other_cols_oi,'variants_col':args.variants_col
            }
        }

        func_dict={
            'bed':query,
            'sumstats':query,
            'list_of_uni_ids':query_uni_ids2
        }

        result_metadata_tables_dict={}
        if args.file_type is not None:
            dfs=parse_dict[args.file_type](**parse_args_dict[args.file_type])
            # counter=0
            for df in dfs:
                # df=df.head(10)
                res=func_dict[args.file_type](conn,df,args.other_cols_oi)
                # print(res)
                for i in res:
                    # print(i)
                    unique_metadatatables=i['metadata_table'].drop_duplicates()
                    for data_type in unique_metadatatables:
                        if data_type not in result_metadata_tables_dict:
                            result_metadata_tables_dict[data_type]=pd.DataFrame()
                        m=query_metadata(conn,i,data_type)
                        result_metadata_tables_dict[data_type]=result_metadata_tables_dict[data_type].append(m)
        else:
            print('no operation specified')
        if args.output_dir is None:
            args.output_dir='.'
        for key in result_metadata_tables_dict.keys():
            result_metadata_tables_dict[key].to_csv(os.path.join(args.output_dir,key+'_metadata.csv'),index=False)
        conn.commit()
        conn.close()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        sys.stderr.write("User interrupted!")
        sys.exit(0)

