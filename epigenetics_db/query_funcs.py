import pandas as pd
def query(conn,df,**query_args_dict):
    df.to_sql('tmp', con=conn,if_exists='replace')
    query="SELECT * FROM loci"
    part1=" JOIN tmp ON loci.chr_name = tmp.chr_name AND loci.start_pos <= tmp.end_pos AND loci.end_pos >= tmp.start_pos limit 100000;"
    part2=' WHERE EXISTS(SELECT * FROM tmp WHERE (loci.chr_name = tmp.chr_name) AND (loci.start_pos <= tmp.start_pos AND loci.end_pos >= tmp.start_pos) OR (loci.start_pos <= tmp.end_pos AND loci.end_pos >= tmp.end_pos)) limit 100000'
    query=query+part1
    # vals=(str(tuple(df['start_pos'].to_list())),)
    result=pd.read_sql_query(query, conn)
    return result

def query_uni_ids(conn,df,**query_args_dict):
    df.to_sql('tmp', con=conn,if_exists='replace')
    query="SELECT loci.*, uni_id,tissue,relevant_gtex_tissue,metadata_table,disease FROM loci"
    part1=" JOIN tmp ON loci.chr_name = tmp.chr_name AND loci.start_pos <= tmp.end_pos AND loci.end_pos >= tmp.start_pos JOIN files on files.file_id=loci.file_id"
    part2=' WHERE EXISTS(SELECT * FROM tmp WHERE (loci.chr_name = tmp.chr_name) AND (loci.start_pos <= tmp.start_pos AND loci.end_pos >= tmp.start_pos))'
    query=query+part1
    # vals=(str(tuple(df['start_pos'].to_list())),)
    result=pd.read_sql_query(query, conn)
    return result


def query_uni_ids2(conn,df,**query_args_dict):
    for i in df.itertuples():
        print(i)
        # i.to_sql('tmp', con=conn,if_exists='replace')
        query="SELECT loci.*,tissue,relevant_gtex_tissue,metadata_table,disease FROM loci"
        part1=" JOIN tmp ON loci.chr_name = ? AND loci.start_pos <= ? AND loci.end_pos >= ? JOIN files on files.file_id=loci.file_id"
        part2=' WHERE EXISTS(SELECT * FROM tmp WHERE (loci.chr_name = tmp.chr_name) AND (loci.start_pos <= tmp.start_pos AND loci.end_pos >= tmp.start_pos))'
        query=query+part1
        query="SELECT loci.*,tissue,relevant_gtex_tissue,metadata_table,disease,file_basename FROM files JOIN loci on loci.file_id=files.file_id WHERE loci.chr_name = {0} AND loci.start_pos <= {1} AND loci.end_pos >= {2}".format(str(i.chr_name),str(i.start_pos),str(i.start_pos))
        result=pd.read_sql_query(query, conn)
        yield result


