#!/usr/bin/env python
from file_parser_funcs import parse_bed, parse_sumstats, parse_uni_ids
from query_funcs import query, query_uni_ids, query_uni_ids2
from sqlalchemy.engine import url
from sqlalchemy.orm import sessionmaker
from sqlalchemy import engine_from_config
from sqlalchemy import create_engine
import random
import argparse
import os
import sys
import epigenetics_db_orm as ep
from epigenetics_db_orm import Base, File, Locus, ChromatinState, TranscriptionFactor, ATACSeq
import pandas as pd
import dask.dataframe as dd
import inspect

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

parser = argparse.ArgumentParser(
    description="edit data in your epigenetics database"
)
parser.add_argument('--database',
    type=str,
    help="database",
    default='/lustre/projects/DTAdb/resources/epigenetics/sqlite_db/epigenetics.db'
)
parser.add_argument('--file_type',
    type=str,
    help="bed,vcf,sumstats etc.",
    default=None
)
parser.add_argument('--file',
    type=str,
    help='File to use as update reference'
)
parser.add_argument('--header',
    type=str2bool,
    help='column containing chromosome',
    default=False
)

parser.add_argument('--delimiter',
    type=str,
    help='tab separated or space?',
    default='\t'
)
parser.add_argument('--chrpos_spec',
    nargs='+',
    help='column to parse chrpos data from- specify column and separators e.g. to say first column split on a colon followed by a hyphen do "0 : -" ',
    default=None
)
parser.add_argument('--chr_col',
    type=int,
    help='column containing chromosome',
    default=0
)
parser.add_argument('--start_col',
    type=int,
    help='column containing start position',
    default=1
)
parser.add_argument('--end_col',
    type=int,
    help='column containing end position',
    default=2
)
parser.add_argument('--liftover',
    type=str2bool,
    help='liftover ',
    default=False
)
parser.add_argument('--lift_from',
    type=str,
    help='liftover from what genome version e.g. GRCh37',
)
parser.add_argument('--lift_to',
    type=str,
    help='liftover from what genome version e.g. GRCh38'
)
parser.add_argument('--variants_col',
    type=str,
    help='column with uni_ids in it',
    default='Variants'
)
parser.add_argument('--compression',
    type=str,
    help='column containing chromosome',
    default=None
)

args = parser.parse_args()
args = parser.parse_args()
db_name=args.database
import sqlite3
conn = sqlite3.connect(db_name)
cursor = conn.cursor()
if args.header:
    header=True
else:
    header=None

try:
    int(args.dict_cols[0])
    args.dict_cols[0]=int(args.dict_cols[0])
except:
    pass

try:
    int(args.dict_cols[1])
    args.dict_cols[1]=int(args.dict_cols[1])
except:
    pass

parse_dict={
    'bed':parse_bed,
    'sumstats':parse_sumstats,
    'list_of_uni_ids': parse_uni_ids
}

print(args.file,args.delimiter,args.header,args.compression,args.liftover,args.lift_from,args.lift_to)
parse_args_dict={
    'bed':{
        'file':args.file,'sep':args.delimiter,'header':args.header,'compression':args.compression
    },
    'sumstats':{
        'file':args.file,'sep':args.delimiter,'header':args.header,'compression':args.compression
    },
    'list_of_uni_ids':{
        'file':args.file,'sep':args.delimiter,'header':args.header,'compression':args.compression,
        'lift':args.liftover,'lift_from':args.lift_from,'lift_to':args.lift_to,'variants_col':args.variants_col
    }
}

func_dict={
    'bed':query,
    'sumstats':query,
    'list_of_uni_ids':query_uni_ids2
}

def requery_states(df):
    pass

if args.file_type is not None:
    for df in parse_dict[args.file_type](**parse_args_dict[args.file_type]):
        res=func_dict[args.file_type](conn,df)
        print(res)
        for i in res:
            print(i)
            metadatatable=i['metadata_table'].drop_duplicates()
            print(metadatatable)
else:
    print('no operation specified')

conn.commit()
conn.close()

